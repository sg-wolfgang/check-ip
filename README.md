# Check IP com PHP

Este é um projeto simples que desenvolvi para utilizar em aula. 

Por meio de um arquivo `php` faço a checagem do IP Público do visitante.

> Obs.: Não há persistencia em nenhum banco de dados, de fato é um exemplo muito simples.

## Utilizando a imagem docker

Para utilizar este exemplo você tem duas opções:

1. Criar a própria imagem
2. Utilizar a minha imagem do docker hub
3. Utilizar o DockerCompose para subir o ambiente

Vou exemplificar os dois modos abaixo.

### Criando a própria imagem

Primeiro, clone este repositório:
```bash
https://gitlab.com/sg-wolfgang/check-ip
```

Depois entre no diretório clonado:
```bash
cd check-ip
```

Execute o build da imagem:
```bash
docker build -t check-ip:latest .
```

Assim que o processo de build terminar, basta executar a imagem criada:
```bash
docker run -dit --name checkip -p 8079:80 check-ip:latest
```

Agora, você pode acessar a aplicação pelo link [http://127.0.0.1:8079](http://127.0.0.1:8079)

### Utilizando a imagem do DockerHub

Basta executar:
```bash
docker run -dit --name checkip -p 8079:80 sgoncalves4linux/check-ip:latest
```

Agora, você pode acessar a aplicação pelo link [http://127.0.0.1:8079](http://127.0.0.1:8079)

### Utilizando DockerCompose


Primeiro, clone este repositório:
```bash
https://gitlab.com/sg-wolfgang/check-ip
```

Depois entre no diretório clonado:
```bash
cd check-ip
```

Então execute o container por meio do comando:
```bash
docker-compose up -d
```

Agora, você pode acessar a aplicação pelo link [http://127.0.0.1:8079](http://127.0.0.1:8079)