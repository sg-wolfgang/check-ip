<!DOCTYPE HTML>
<html>
   <head>
      <title>Verificador de IP Público</title>
      <meta charset='UTF-8'>
      <style type='text/css'>
         body{background:#ffffff;color:#000000;font-family:comic sans ms;margin:0px;width:98%;padding:1%;}
         h1{color:#000000;font-size:250%;text-align:center;clear:both;}
         h3{color:#ff0000;font-size:250%;text-align:center;clear:both;}
         h2{color:#000000;font-size:150%;text-align:center;clear:both;}
         h4{color:#000000;font-size:80%;text-align:center;clear:both;}
         h6{color:#000000;font-size:50%;text-align:center;clear:both;}
         img{float:left;width:20%;height:20%;margin:5px;border:0px solid #228800;}
         p{color:#000000;font-size:100%;text-align:center;}
         a:link, a:visited{color:#4f3eee;}
         marquee{width:98%;padding:1%;color:#000000;background:#ffffff;font-size:100%;}
      </style>
   </head>
   <body>
      <h1>Verificador de IP Público</h1>
      <h2><a href="https://sgoncalves.tec.br" target="_blank">By: S. Gonçalves</a></h2>
      <p>Seu endereço IP público é:</p>
      <h3><?php include('ip.php') ?></h3>
      <h4>É importante lembrar que este é somente um arquivo simples em HTML com PHP que verifica seu ip, pode ficar tranquilo ok?</h4>
      <p>Dúvidas e sugestões? Acesse o repositório do projeto <a href="https://gitlab.com/sg-wolfgang/check-ip" target="_blank">clicando aqui</a></p>
   </body>
</html>